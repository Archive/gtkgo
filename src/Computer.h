/* gtkgo 0.0.10 (Computer.h) - 05/17/99
 * Copyright (C) 1998, 1999  Norbert de Jonge
 * (hack@altavista.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this game; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef COMPUTER_H
#define COMPUTER_H

#include "main.h"

int   ibuttonc,
      irandom,
      ireturned,
      ifiledes,
      ido,
      itry,
      itry2,
      itry3,
      iok,
      iok2,
      iok3,
      i10p,
      i50p,
      iabove,
      iunder,
      iright,
      ileft,
      idid,
      ictemp,
      ictemp2,
      ineed,
      istopit,
      itheone,
      i30moves,
      iok4;
int   ieye[6][6];
float fnumber;
char  thefile[100];
char  schar[1];
char  ccheck,
      cnotccolor;
char  ssides[10];
char  sctemp[10];
int   itriedb[362];

#endif
