/* gtkgo 0.0.10 (ReceiveData.h) - 05/17/99
 * Copyright (C) 1998, 1999  Norbert de Jonge
 * (hack@altavista.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this game; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef RESCEIVEDATA_H
#define RESCEIVEDATA_H

#include "main.h"

char recvline[256];
int  itemp2,
     iinetmove,
     iinetcreate,
     iinethand,
     iinetgame,
     iinetresign,
     iinetpls,
     iinetundo,
     isetclient;
char sinetmove[4];
char sinettemp[4];
char sothername[11];
char sinettemp2[11];
char shandicap[3];
char sinettemp3[3];
char sshowthis[2];
char sinettemp4[11];
char snamew[11];
char snameb[11];

#endif
