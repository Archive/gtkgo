/* gtkgo 0.0.10 (MainWindow.h) - 05/17/99
 * Copyright (C) 1998, 1999  Norbert de Jonge
 * (hack@altavista.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this game; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "main.h"

GtkWidget     *box1;
GtkWidget     *box2;
GtkWidget     *box3;
GtkWidget     *box4;
GtkWidget     *table1;
GtkWidget     *buttontop;
GtkWidget     *buttontop2;
GtkWidget     *buttontop3;
GtkWidget     *buttontop4;
GtkWidget     *buttontop5;
GtkWidget     *buttontop6;
GtkWidget     *buttontop7;
GtkWidget     *buttontop8;
GdkPixmap     *toppixmap;
GdkPixmap     *top2pixmap;
GdkPixmap     *top3pixmap;
GdkPixmap     *top4pixmap;
GdkPixmap     *top5pixmap;
GdkPixmap     *top6pixmap;
GdkPixmap     *top7pixmap;
GdkPixmap     *top8pixmap;
GtkWidget     *toppixmapwid;
GtkWidget     *top2pixmapwid;
GtkWidget     *top3pixmapwid;
GtkWidget     *top4pixmapwid;
GtkWidget     *top5pixmapwid;
GtkWidget     *top6pixmapwid;
GtkWidget     *top7pixmapwid;
GtkWidget     *top8pixmapwid;
char          thepixmap[200];
GdkImlibImage *im2;
gint          w2,
              h2;
GtkWidget     *statbar;
GtkWidget     *menubar;

#endif
