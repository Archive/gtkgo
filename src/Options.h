/* gtkgo 0.0.10 (Options.h) - 05/17/99
 * Copyright (C) 1998, 1999  Norbert de Jonge
 * (hack@altavista.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this game; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef OPTIONS_H
#define OPTIONS_H

#include "main.h"

GtkWidget     *changebox1;
GtkWidget     *changebox2;
GtkWidget     *changetable1;
GtkWidget     *changebutton;
GtkWidget     *changebutton2;
GtkWidget     *changebutton3;
GtkWidget     *changebutton4;
GtkWidget     *changebutton5;
GtkWidget     *changebutton6;
GtkWidget     *changebutton8;
GtkWidget     *changebutton10;
GtkWidget     *changebutton12;
GtkWidget     *changebuttonx;
GdkImlibImage *im3;
gint          w3,
              h3;
GdkPixmap     *cbpixmap;
GdkPixmap     *cbpixmap2;
GdkPixmap     *cbpixmap3;
GdkPixmap     *cbpixmap4;
GdkPixmap     *cbpixmap5;
GdkPixmap     *cbpixmap6;
GdkPixmap     *cbpixmap8;
GdkPixmap     *cbpixmap10;
GdkPixmap     *cbpixmap12;
GdkPixmap     *cbpixmapx;
GtkWidget     *cbpixmapwid;
GtkWidget     *cbpixmapwid2;
GtkWidget     *cbpixmapwid3;
GtkWidget     *cbpixmapwid4;
GtkWidget     *cbpixmapwid5;
GtkWidget     *cbpixmapwid6;
GtkWidget     *cbpixmapwid8;
GtkWidget     *cbpixmapwid10;
GtkWidget     *cbpixmapwid12;
GtkWidget     *cbpixmapwidx;
char          cThePng2[200];
GtkWidget     *menu;
GtkWidget     *menu2;
GtkWidget     *menu_items;

#endif
